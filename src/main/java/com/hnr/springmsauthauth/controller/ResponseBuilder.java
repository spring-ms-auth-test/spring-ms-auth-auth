package com.hnr.springmsauthauth.controller;

import com.hnr.springmsauthauth.model.ResponseModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;

public class ResponseBuilder {

    protected <T> ResponseEntity<ResponseModel> makeFinalResponse(int code, String msg, T data, HashMap<String, Object> metaData) {
        return new ResponseEntity<>(new ResponseModel<>(code, msg, data, metaData), HttpStatus.OK);
    }

    protected <T> ResponseEntity<ResponseModel> makeSuccessResponse(T data) {
        return makeFinalResponse(ResponseModel.STATUS_SUCCESS, ResponseModel.MSG_SUCCESS, data, new HashMap<>());
    }

    protected ResponseEntity<ResponseModel> makeInvalidParamsResponse() {
        return makeFinalResponse(ResponseModel.STATUS_INVALID_PARAMS, ResponseModel.MSG_INVALID_PARAMS, null, new HashMap<>());
    }
}
