package com.hnr.springmsauthauth.controller;

import com.hnr.springmsauthauth.model.ResponseModel;
import com.hnr.springmsauthauth.model.RoleEntity;
import com.hnr.springmsauthauth.model.UserEntity;
import com.hnr.springmsauthauth.model.UserRegister;
import com.hnr.springmsauthauth.repository.RoleRepository;
import com.hnr.springmsauthauth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("user")
public class UserController extends ResponseBuilder {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    private static final String secret = "dhfw7y48t78493758vb38753v783485687487675nv7";

    @GetMapping("/{id}")
    public UserEntity getById(@PathVariable long id) {
        System.out.println("Get user: " + id);
        return userRepository.findById(id).orElse(new UserEntity());
    }

    @GetMapping("/user-by-id/{id}")
    public UserEntity getUserByIdSecret(@PathVariable long id, @RequestParam String secret) {

        if(!UserController.secret.equals(secret)){
            return null;
        }

        System.out.println("Get user: " + id);
        return userRepository.findById(id).orElse(new UserEntity());
    }

    @GetMapping("/principal")
    public Principal getUserPrincipal(Principal principal) {
        return principal;
    }

    @GetMapping("/current")
    public ResponseEntity<ResponseModel> getCurrentUser(Principal principal) {
        UserEntity userEntity = userRepository.findByUsername(principal.getName());
        return makeSuccessResponse(userEntity);
    }

    @GetMapping("/principal-name")
    public String getUserPrincipalName(Principal principal) {
        return principal.getName();
    }

    @GetMapping("/home")
    public String getHome() {
        return "User home page";
    }

    @GetMapping("/code")
    public String getCode(@RequestParam String code) {
        return code;
    }

    @PostMapping("/register")
    public ResponseEntity<ResponseModel> register(@RequestBody UserRegister userRegister){

        if (userRegister == null) {
            return makeInvalidParamsResponse();
        }

        List<RoleEntity> roles = new ArrayList<>();
        roles.add(roleRepository.findByName(RoleEntity.ROLE_USER));
        userRegister.setId(0);
        UserEntity userEntity = new UserEntity(userRegister);
        userEntity.setRoles(roles);

        userRepository.saveAndFlush(userEntity);

        return makeSuccessResponse(userEntity);
    }
}
