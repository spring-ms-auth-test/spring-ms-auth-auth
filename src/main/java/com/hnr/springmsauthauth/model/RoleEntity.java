package com.hnr.springmsauthauth.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_role")
public class RoleEntity {

    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_USER = "USER";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
}
