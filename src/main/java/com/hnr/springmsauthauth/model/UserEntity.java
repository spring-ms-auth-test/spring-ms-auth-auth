package com.hnr.springmsauthauth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_user")
public class UserEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String fullName;
    private String username;

    @JsonIgnore
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<RoleEntity> roles;

    public UserEntity() {
    }

    public UserEntity(UserRegister userRegister) {
        this.id = userRegister.getId();
        this.username = userRegister.getUsername();
        this.password = userRegister.getPassword();
        this.fullName = userRegister.getFullName();
    }
}
