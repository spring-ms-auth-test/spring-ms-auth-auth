package com.hnr.springmsauthauth.repository;

import com.hnr.springmsauthauth.model.RoleEntity;
import com.hnr.springmsauthauth.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    RoleEntity findByName(String name);
}
